Categories:Connectivity,Multimedia
License:LGPLv3
Web Site:https://benoitduffez.github.io/AndroidCupsPrint/
Source Code:https://github.com/BenoitDuffez/AndroidCupsPrint/tree/fdroid
Issue Tracker:https://github.com/BenoitDuffez/AndroidCupsPrint/issues

Auto Name:Android CUPS Print
Summary:This app allows you to print directly from Android to your CUPS server
Description:
Have a local printer shared over CUPS or IPP? Then this app allows you to
directly print to it from your Android device.

This app just provides a Print Service to Android. This means that once it's
installed, you have to enable it from your 'Print' section of the settings app
of you device. Once the service is enabled, the printers are automatically
discovered using the mDNS protocol. You can print anything you want from any
app, as long as the print service is enabled.

Port of cups4j to Android. Original work was created by Jon Freeman, it included
an app that reacts to the SEND intent to print documents.
.

Repo Type:git
Repo:https://github.com/BenoitDuffez/AndroidCupsPrint

Build:1.2,4
    commit=ae2f04270c374c89cad312446cee68bd97c99a8f
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.2
Current Version Code:4
